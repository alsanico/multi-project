package gr.savsoft.api.resource;

import java.math.BigDecimal;

import gr.savsoft.domain.resource.Resource;

public interface IResourceService {

	Resource findById (BigDecimal id);
	
	Resource findbyName (String name);
}
